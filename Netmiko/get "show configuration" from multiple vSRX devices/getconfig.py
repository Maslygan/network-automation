import netmiko

devices = """
172.25.11.1
172.25.11.2
""".strip().splitlines()

device_type = 'juniper'
username = 'lab'
password = 'lab123'

for device in devices:
        print('~' * 79)
        print('Connecting to device:', device)
        connection = netmiko.ConnectHandler(ip=device, device_type=device_type,
                                            username=username, password=password)
        file = open(device + '_config.json', 'w')
        output = (connection.send_command('show configuration'))
        print('Printing file for device:', device)
        file.write(output)
        file.close()
        connection.disconnect()