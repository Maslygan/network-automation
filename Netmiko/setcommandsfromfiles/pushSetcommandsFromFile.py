import netmiko
import json

with open('devices.json') as dev_file:
        devices = json.load(dev_file)

with open('setvSRX1.txt', 'r') as set_file:
        setvSRX1 = set_file.read()
        
with open('setvSRX2.txt', 'r') as set_file:
        setvSRX2 = set_file.read()

for device in devices:
        print('~' * 79)
        print('Connecting to device:', device['host'], 'on IP:', device['ip'])
        connection = netmiko.ConnectHandler(**device)
        file = open(device['host'] + '_beforeconfig.json', 'w')
        output = (connection.send_command('show configuration'))
        file.write(output)
        file.close()
        print(connection.send_command('show route terse'))
        if device['host'] == 'vSRX-1':
                print('Setting interface(s)')
                print(connection.send_config_set(setvSRX1,
                                              exit_config_mode=False))
                print(connection.commit(and_quit=True)) # Exit configuration mode
        elif device['host'] == 'vSRX-2':
                print('Setting interface(s)')
                print(connection.send_config_set(setvSRX2,
                                              exit_config_mode=False))
                print(connection.commit(and_quit=True)) # Exit configuration mode
        print(connection.send_command('show route terse'))
        file = open(device['host'] + '_afterconfig.json', 'w')
        output2 = (connection.send_command('show configuration'))
        file.write(output2)
        print('Printing file for device:', device['host'])
        file.close()
        
        connection.disconnect()

