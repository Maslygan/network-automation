import netmiko
import json


with open('devices.json') as dev_file:
        devices = json.load(dev_file)



vSRX1Setcommands =    ['set interfaces ge-0/0/4 unit 0 family ' + \
                   'inet address 192.168.4.1/24',
                   'set interfaces ge-0/0/6 unit 0 family ' + \
                   'inet address 192.168.6.1/24']

vSRX2Setcommands =    ['set interfaces ge-0/0/4 unit 0 family ' + \
                   'inet address 192.168.4.2/24',
                   'set interfaces ge-0/0/6 unit 0 family ' + \
                   'inet address 10.10.6.1/24']


for device in devices:
        print('~' * 79)
        print('Connecting to device:', device['host'], 'on IP:', device['ip'])
        connection = netmiko.ConnectHandler(**device)
        file = open(device['host'] + '_beforeconfig.json', 'w')
        output = (connection.send_command('show configuration'))
        file.write(output)
        file.close()
        #output1 = (connection.send_command('show version| match Hostname'))
        #print(connection.send_command('show route terse'))
        if device['host'] == 'vSRX-1':
                print('Setting interface(s)')
                print(connection.send_config_set(vSRX1Setcommands,
                                              exit_config_mode=False))
                print(connection.commit(and_quit=True)) # Exit configuration mode
        elif device['host'] == 'vSRX-2':
                print('Setting interface(s)')
                print(connection.send_config_set(vSRX2Setcommands,
                                              exit_config_mode=False))
                print(connection.commit(and_quit=True)) # Exit configuration mode
        print(connection.send_command('show route terse'))
        #print(output1)
        file = open(device['host'] + '_afterconfig.json', 'w')
        output2 = (connection.send_command('show configuration'))
        file.write(output2)
        print('Printing file for device:', device['host'])
        file.close()
        
        connection.disconnect()

