import netmiko
import json


with open('devices.json') as dev_file:
        devices = json.load(dev_file)

for device in devices:
        print('~' * 79)
        print('Connecting to device:', device['ip'])
        connection = netmiko.ConnectHandler(**device)
        file = open(device['ip'] + '_config.json', 'w')
        output = (connection.send_command('show configuration'))
        print('Printing file for device:', device['ip'])
        file.write(output)
        file.close()
        connection.disconnect()